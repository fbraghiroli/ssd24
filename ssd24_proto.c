/*
 * ssd24_proto.c
 *
 *  Created on: Apr 22, 2019
 *      Author: federico
 */

#include "ssd24.h"
#include "ssd24_proto.h"
#include <stddef.h>

static size_t payload_size(struct ssd_frame *f)
{
	return f->frame_size-offsetof(struct ssd_frame, data);
}

/* ap node id: 5bit -> max 32 ap in the leaf range,
 * ap pipe n: useless?  */

static int do_join_reply(struct ssd_i *i_p, struct ssd_frame *f)
{
	/* leaf node id: 5bit
	 * ap node id + pipe n: 5 bit + 3 bit
	 * ap pipe addr: 5 byte
	 */

	uint8_t *pp;
	size_t ps;

	ps = payload_size(f);
	if (ps != 7)
		return -SERR_FRAME_INVALID;

	pp = f->data;
	if (pp[0] != i_p->dev_id) {
		/* this reply is not for us */
		/* TODO: return specific code? */
		return 0;
	}
	i_p->ld.ap_id = pp[1] >> 3;
	i_p->ld.ap_pn = pp[1] & 0b00000111;
	/* TODO: check if address is valid */
	memcpy(i_p->ld.ap_paddr, &pp[2], SSD_NODE_ADDR_SIZE);

	/* send back the ack */
}

int process_ap(struct ssd_i *i_p, int pipe, const void *buf, size_t len)
{
	return 0;
}

int process_leaf(struct ssd_i *i_p, int pipe, const void *buf, size_t len)
{
	struct ssd_frame f;
	uint8_t *fp;
	/*
	 * - decodificare pacchetto
	 * - verificare coerenza con pipe su cui e' stato ricevuto
	 * - iterazione macchina a stati*/

	if (len > SSD_FRAME_MAX_LEN)
		return -SERR_FRAME_LEN;
	fp = buf;
	f.vm = *((uint16_t *)buf);
	if (SSD_GET_MAGIC(f.vm) != SSD_FRAME_MAGIC)
		return -SERR_FRAME_INVALID;
	/* TODO: handle different versions */
	if (SSD_GET_VER(f.vm) != SSD_PROTOCOL_VERSION)
		return -SERR_FRAME_INVALID;
	f.frame_size = fp[1];
	if (f.frame_size != len)
		return -SERR_FRAME_INVALID;
	f.cmd_id = fp[2];
	f.chk_mask = fp[3];
	f.data = &fp[4];

	switch(f.cmd_id) {
	case SSD_CMD_JOIN_REPLY:
		if (i_p->lf_s != LF_S_JOIN_REQ)
			return -SERR_FRAME_UNEXP;
		if (pipe != SSD_BCAST_PIPE)
			return -SERR_CMD_INVALID;
		else
			do_join_reply(i_p, &f);
		break;
	case SSD_CMD_JOIN_STATUS_REPLY:
		break;
	case SSD_CMD_POOL_REPLY:
		break;
	default:
		return -SERR_CMD_INVALID;
	}
}
