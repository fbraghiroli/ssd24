/*
 * ssd24.h
 *
 *  Created on: Apr 21, 2019
 *      Author: federico
 */

#ifndef SSD24_SSD24_H_
#define SSD24_SSD24_H_

/* Try to adhere to C99 standard */

#include <stdlib.h>

/* NOTE:
 * The only thing that must unique and provided from the user is the control
 * address of each node (ap and leaf) within the same net. */

#define SSD_MAX_PAIR		5

enum ssd_err {
	SERR_NONE = 0,
	SERR_LIMIT,
	SERR_NOMEM,
	SERR_UNPAIRED,
	SERR_PAIR_ID_IN_USE,
	SERR_PAIR_FULL,
	SERR_INVAL,
	SERR_FRAME_LEN,
	SERR_FRAME_INVALID,
	SERR_FRAME_UNEXP,
	SERR_CMD_INVALID
};

enum ssd_node_t {
	SSD_NODE_AP,
	SSD_NODE_LEAF
};

/* write hooks must encapsulate ssd protocol to the user specific packet protocol */
struct ssd_hooks {
	int (*write)(uint8_t dev_id, const void *buf, size_t len);
	int (*conf_pipe)(uint8_t dev_id, uint8_t p_n, const char *addr,
			uint8_t autoack);
	void (*wait)(unsigned int t);
	int (*pair_cbk)(const char *pair_addr);
};

/* Init internal structures, allocate memory, etc...
 * Return negative value if error (ssd_err) or the instance id (i_id).
 * Can be called multiple times to handle different interfaces.
 *
 * dev_id: number that helps application code to keep track of the device in use
 * listen_addr: address on which listen for broadcast/control messages (on pipe 0) */
int ssd_init(uint8_t dev_id, const char *listen_addr, enum ssd_node_t t, struct ssd_hooks *h);

/* Set the control address (for the ap is the listen address, for leaf nodes
 * is the address for ap replies)
 * (TODO: maybe this can be done in application space...)
 */
int ssd_set_ctrl_addr(const char *ctrl_addr);

/* Must be called each time something is received from nrf24l01 interface. */
int ssd_process(int i_id, int pipe, const void *buf, size_t len);

/* instance id: the one returned from init
 * node type is chosen during init
 * node id: must be unique within ap <--> leaf link
 *
 * return 0 or error code
 */
int ssd_join_req(int i_id, uint8_t nid);

/* async version (returns immediately) */
int ssd_join_req_async(int i_id, uint8_t nid);

/* Only works for LEAF node (if called on ap, addr is zero filled) */
int ssd_get_pair_addr(int i_id, const char *addr);

int ssd_get_ap_id(int i_id);

#endif /* SSD24_H_ */
