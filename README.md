# Super Simple Discovery protocol for nrf24l01 (SSD24)

## WORK IN PROGRESS

Some the following things have not been implemented
and could change in future.

* Designed for nrf24l01.
* Access point (AP) + leaf (or client).
* Use pipe 0 with a common broadcast address common for all the nodes.
* Access point send a bacon to broadcast address.
* Client asking to join the network will receive an address linked to a free pipe of the AP.
* AP keep a list of joined client, other application can query this list to create mesh network.
* This protocol can work as a base for other stacks.
* It must be simple! No more than 4 nodes (1 for each pipe).
* It must be portable!