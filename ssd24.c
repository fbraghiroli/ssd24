/*
 * ssd24.c
 *
 *  Created on: Apr 21, 2019
 *      Author: federico
 */

#include "ssd24.h"
#include "ssd24_proto.h"

#define SSD_MAX_INSTANCES	2


/* Critical sections must be handled outside */

static struct ssd_data {
	struct ssd_i *i_list[SSD_MAX_INSTANCES];
} priv;

int ssd_init(uint8_t dev_id, const char *listen_addr, enum ssd_node_t t,
	     struct ssd_hooks *h)
{
	/* TODO: check args */

	struct ssd_i **i_p;
	int i;

	for (i_p = priv->i_list, i = 0; i_p != priv.i_list[SSD_MAX_INSTANCES];
	     i_p++, i++)
		if (!(*i_p))
			break;

	if (i_p == priv.i_list[SSD_MAX_INSTANCES])
		return -SERR_LIMIT;

	if (!(*i_p = malloc(sizeof(struct ssd_i))))
		return -SERR_NOMEM;

	(*i_p)->dev_id = dev_id;
	memcpy((*i_p)->bcast_addr, listen_addr,
		SSD_NODE_ADDR_SIZE*sizeof((*i_p)->bcast_addr[0]));
	(*i_p)->type = t;
	(*i_p)->h = *h;

	if (t == SSD_NODE_LEAF)
		(*i_p)->lf_s = LF_S_INIT;

	return i;
}

int ssd_process(int i_id, int pipe, const void *buf, size_t len)
{
	struct ssd_i *i_p;
	int ret;
	/* TODO: check args */

	if (i_id >= SSD_MAX_INSTANCES || !priv.i_list[i_id])
		return -SERR_INVAL;

	i_p = priv.i_list[i_id];

	if (i_p->type == SSD_NODE_AP)
		ret = process_ap(i_p, pipe, buf, len);
	else
		ret = process_leaf(i_p, pipe, buf, len);
}
