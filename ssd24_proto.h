/*
 * proto.h
 *
 *  Created on: Apr 21, 2019
 *      Author: federico
 */

#ifndef SSD24_PROTO_H_
#define SSD24_PROTO_H_

#define SSD_PROTOCOL_VERSION		1
/* Which pipe is used as broadcast / control channel */
#define SSD_BCAST_PIPE			0

#define SSD_NODE_ADDR_SIZE		5

/* Assume user packet protocol of just 1 byte (protocol ID) */
#define SSD_FRAME_MAX_LEN		31
#define SSD_FRAME_MAGIC			0x24d

#define SSD_CMD_JOIN_REQ		0x01
#define SSD_CMD_JOIN_REPLY		0x02
#define SSD_CMD_JOIN_CONFIRM		0x03
#define SSD_CMD_JOIN_STATUS_REQ		0x04
#define SSD_CMD_JOIN_STATUS_REPLY	0x05
#define SSD_CMD_POOL_REQ		0x06
#define SSD_CMD_POOL_REPLY		0x07
#define SSD_CMD_BEACON			0x08

#define SSD_GET_MAGIC(vm) ((vm >> 6) & 0x03ff)
#define SSD_GET_VER(vm) (vm & 0x3f)

enum leaf_states {
	LF_S_INIT,
	LF_S_UNJOINED,
	LF_S_JOIN_REQ,
	LF_S_JOINED,
};

/* leaf specific data */
struct ssd_leafd {
	int16_t ap_id;	/* -1 -> no ap known / joined */
	uint8_t ap_pn;  /* ap pipe number */
	uint8_t ap_paddr[SSD_NODE_ADDR_SIZE];
};

struct ssd_i {
	uint8_t dev_id; /* 5bit */
	//uint8_t l_addr[SSD_NODE_ADDR_SIZE];
	struct ssd_leafd ld;
	uint8_t bcast_addr[SSD_NODE_ADDR_SIZE];	/* the same for all nodes */
	enum ssd_node_t type;
	struct ssd_hooks h;
	enum leaf_states lf_s;
};

/* This protocol relies on hardware crc */

struct ssd_frame {
	uint16_t vm;	/* version magic: 10bit magic + 6 bit version */
	uint8_t frame_size;
	uint8_t cmd_id;
	uint8_t chk_mask;
	void *data;
};

int process_ap(struct ssd_i *i_p, int pipe, const void *buf, size_t len);
int process_leaf(struct ssd_i *i_p, int pipe, const void *buf, size_t len);

#endif /* ORTO_SSD24_PROTO_H_ */
